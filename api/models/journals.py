from pydantic import BaseModel
from datetime import date
from typing import List


class JournalIn(BaseModel):
    account_id: int
    date: date
    content: str

class JournalOut(JournalIn):
    id: int

class JournalListOut(BaseModel):
    journals: List[JournalOut]