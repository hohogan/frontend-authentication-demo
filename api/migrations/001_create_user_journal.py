steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE account (
            account_id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(50) UNIQUE NOT NULL,
            fact TEXT NOT NULL,
            email VARCHAR(50) NOT NULL,
            password TEXT NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE user;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE journal (
            id SERIAL PRIMARY KEY NOT NULL,
            account_id INT REFERENCES account(account_id),
            date DATE NOT NULL,
            content TEXT NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE journal;
        """
    ]
]
