from models.accounts import AccountIn, AccountOut, AccountWithHashedPassword, AccountFull
from psycopg_pool import ConnectionPool
from psycopg.errors import UniqueViolation
from fastapi import HTTPException

import os

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class AccountRepo():
    def create(
            self,
            account: AccountWithHashedPassword
    ) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(
                        """
                        INSERT INTO account
                        (username, fact, email, password)
                        VALUES (%s, %s, %s, %s)
                        RETURNING ACCOUNT_ID, USERNAME, FACT, EMAIL
                        """,
                        [account.username, account.fact, account.email, account.hashed_password],
                    )
                    ac = cur.fetchone()
                    
                    return AccountOut(
                        account_id=ac[0],
                        username=ac[1],
                        fact=ac[2],
                        email=ac[3],
                    )
                except UniqueViolation as e: 
                    raise HTTPException(status_code=422, detail=str(e))


    def get(
            self,
            username: str
    ) -> AccountFull:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT account_id, username, fact, email, password
                    FROM account
                    WHERE username = %s;
                    """,
                    [username],
                )
                ac = cur.fetchone()
                if ac is None:
                    raise HTTPException(status_code=404, detail=f"No account found for {username}")
                else:
                    try:
                        return AccountFull(
                            account_id=ac[0],
                            username=ac[1],
                            fact=ac[2],
                            email=ac[3],
                            hashed_password=ac[4],
                        )
                    except Exception as e:
                        raise Exception("Error:", e)